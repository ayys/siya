FROM python:2-alpine

COPY alms /alms
WORKDIR /alms

#  install required dependencies
RUN pip install -r requirements.txt && mkdir /run/nginx && apk add nginx

COPY siya-pustakalaya.conf /etc/nginx/conf.d/default.conf

CMD sh ./start.sh
