# -*- coding: utf-8 -*-


from django.conf.urls import url

urlpatterns = [
    url(r'^/(?P<accession_number>[0-9]+)$', 'barcode.views.generate', name="generateBarcode"),
        ]
