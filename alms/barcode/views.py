from django.shortcuts import render

from settings.models import addGlobalContext
from django.contrib.auth.decorators import login_required

from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

# Create your views here.

@login_required(login_url="/user/login/")
def edit_field(request, accession_number):
    return render(request,
                  "miscFields/edit_field.html",
                  addGlobalContext(
                      {
                          "state": state,
                          "field_id": field_id,
                          "field": field
                      }))
