# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0006_moduser_acitve'),
    ]

    operations = [
        migrations.RenameField(
            model_name='moduser',
            old_name='acitve',
            new_name='active',
        ),
    ]
