# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0005_auto_20170302_1336'),
    ]

    operations = [
        migrations.AddField(
            model_name='moduser',
            name='acitve',
            field=models.BooleanField(default=True),
        ),
    ]
